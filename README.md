# Fancontrol packages for the Arch Linux

<!-- badges: start -->
<!-- badges: end -->

## Usage

### Add repository

Add the following code to `/etc/pacman.conf`:

``` toml
[fancontrol]
SigLevel = PackageOptional
Server = https://aur1.gitlab.io/$repo/$arch
```

### List packages

To show actual packages list:

``` bash
pacman -Sl fancontrol
```

### Install packages

To install package:

``` bash
pacman -Syy
pacman -S fancon-git
```
